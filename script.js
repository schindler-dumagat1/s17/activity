// console.log("Hello World!")

// addStudent Function
let student = []
function addStudent(name) {
	student.push(name)
	console.log(`${name} was added to the student's list.`)
}

// countStudents Function
function countStudents() {
	console.log(`There are a total of ${student.length} students enrolled.`)
}

// printStudents Function
function printStudents() {
	student.sort();

	student.forEach(
		 function(enrollees){
		 	console.log(enrollees)
		 }
	)
}

// findStudent Function
function findStudent(studentName) {
	let studentSearch = student.filter(
			function (name) {
				return (name.toLowerCase().includes(studentName.toLowerCase()))
			}
		)

	if(studentSearch.length == 1) {
		console.log(`${studentSearch} is an Enrollee`);
	} else if (studentSearch.length > 1) {
		console.log(`${studentSearch.sort().join()} are enrollees`);
	} else {
		console.log(`No student found with the name ${studentName}`)
	}
}

// Stretch Goals

// addSection Function
function addSection(section) {
	let sectioned = student.map(
			function(name) {
				return (`${name} - section ${section}`)
			}
		)
	console.log(sectioned);
}

// removeStudents Function
function removeStudent(name) {
	let firstLetter = name.slice(0, 1).toUpperCase()
	let newName = firstLetter + name.slice(1)
	let indexToRemove = student.indexOf(newName)
	student.splice(indexToRemove, 1)
	console.log(`${newName} was removed from the student's list.`)
}
